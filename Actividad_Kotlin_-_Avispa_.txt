/*Integrantes:
 * Sebastian Contreras - Codigo: 54371
 * Julian Vasquez - Codigo: 45032
 */

fun main() {
    
    val alfarera = AvispaAlfarera()
    println ("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    println("El tipo de Avispa Alfarera tiene las siguientes caracteristicas:")
    println ("el color de la avispa alfarera es "+ alfarera.color +",")
    println ("se ubica en el continente de "+ alfarera.continenteUbicacion +",")
    println ("aproximadamente una cantidad de "+ alfarera.cantidad + " millones,")
    alfarera.doblarAlas()
    alfarera.alimentar()
    println ("") 
    val nectar = AvispaDelPolen()
    println ("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    println("El tipo de Avispa del Polen tiene las siguientes caracteristicas:")
    println ("el color de la avispa del Polen es "+ nectar.color +",")
    println ("se ubica en el continente de "+ nectar.continenteUbicacion +",")
    println ("aproximadamente una cantidad de "+ nectar.cantidad + " millones,")
    nectar.cavarSuelo()
    nectar.alimentar() 
    
}
interface Avispa {
   
    var sonPeligrosas: Boolean
    var color : String
    var continenteUbicacion: String
    var cantidad: Int
    fun alimentar()
}

class AvispaAlfarera:Avispa{
    override var sonPeligrosas: Boolean = true
    override var color: String = "Negro"
    override var continenteUbicacion: String="Sudafrica"
    override var cantidad: Int = 25000000
    fun doblarAlas(){
        println("pueden doblar sus alas,")
    }
    override fun alimentar() {
        println("y se alimentan de orugas o larvas de escarabajos.")
    
    }
}

class AvispaDelPolen:Avispa{
    override var sonPeligrosas: Boolean = false
    override var color: String = "Amarillo"
    override var continenteUbicacion: String="Sudamerica"
    override var cantidad: Int = 3000000
    fun cavarSuelo(){
        println("cavan el suelo para esconderese,")
    }
    override fun alimentar() {
        println("y se alimentan exclusivamente del polen y el n�ctar de las flores.")
    }
    
}